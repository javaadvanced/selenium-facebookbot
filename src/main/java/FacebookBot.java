import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.awt.event.KeyEvent;

public class FacebookBot {

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "/home/posa/Downloads/chromedriver_linux64/chromedriver");

        ChromeDriver driver = new ChromeDriver();

        //Lansare site
        driver.get("https://www.facebook.com");

        //Identificare input-uri login
        WebElement emailInput = driver.findElement(By.id("email"));
        WebElement passInput = driver.findElement(By.id("pass"));

        //IntrodUcere user/pass
        emailInput.sendKeys("sapobogdan@gmail.com");
        passInput.sendKeys("gica123");

        //Clik pe butonul de login
        driver.findElementByCssSelector("input[type=submit]").submit();

        //Go to facebook home
        driver.get("https://www.facebook.com");
        Thread.sleep(2000);

        //Scroll 500 px
        JavascriptExecutor jse = ((JavascriptExecutor) driver);
        jse.executeScript("window.scrollTo(0, 500)");

        //Click pe primul buton de tip 'Like'
        WebElement firstLikeButton = driver.findElementsByClassName("UFILikeLink").get(0);
        firstLikeButton.click();


    }
}
